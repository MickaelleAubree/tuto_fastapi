from pydantic import BaseModel
from typing import Optional


class Blog(BaseModel):
    title: str
    body: str


class ShowBlog(BaseModel):
    title: str
    body: str
    class Config():
        orm_mode = True


# User class
class User(BaseModel):
    name: str
    email: str
    password: str


class ShowUser(BaseModel):
    name: str
    email: str
    class Config():
        orm_mode = True


# Login
class Login(BaseModel):
    username: str
    password: str


# JWT Token
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None
