from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel
import uvicorn

app = FastAPI()


@app.get('/')
def index():
    return {'data': {'name': 'Micka-Hell'}}


@app.get('/about')
def about():
    return {'date': 'about page haha lol'}


@app.get('/blog')
def the_blog(limit=10, published: bool = True, sort: Optional[str] = None):
    # only get 10 published blogs
    if published:
        return {'data': f'{limit} published blogs from the list'}
    else:
        return {'data': f'{limit} blogs from the list'}


@app.get('/blog/unpublished')
def unpublished():
    return {'data':'all unpublished blogs'}


@app.get('/blog/{id}')
def show(id: int):  # if data type of parameter 'id' is not defined, it will be automatically an str
    # fetch blog with id = id
    return {'data': id}


@app.get('/blog/{id}/comments')
def comments(id, limit=10):
    # fetch comments of blog with id = id
    #  return limit
    return {'data': {'1', '2'}}

# CREATION OF MODEL


class Blog(BaseModel):
    title: str
    body: str
    published_at: Optional[bool]


@app.post('/blog')
def post_blog(blog: Blog):
    return {'data': f"Blog is created with {blog.title}"}

# use to debug
# if __name__ == "__main__":
#    uvicorn.run(app,host="127.0.0.1", port=9000)
